/** @jsx jsx */
import { jsx, css } from "@emotion/core"

export const VendorPrefixFirst = () => {
  const style = css`
    background-color: coral;
    -webkit-border-radius: 30px 10px;
    border-radius: 30px 10px;
    padding: 1rem;
  `

  return <div css={style}>Vendor Prefix First</div>
}

export const VendorPrefixLast = () => {
  const style = css`
    background-color: coral;
    /* stylelint-disable */
    border-radius: 30px 10px;
    -webkit-border-radius: 30px 10px;
    /* stylelint-enable */
    padding: 1rem;
  `

  return <div css={style}>Vendor Prefix Last</div>
}

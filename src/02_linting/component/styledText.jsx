/** @jsx jsx */
import { jsx, css } from "@emotion/core"

export const Expected = () => {
  const style = css`
    background-color: coral;
    color: blue;
    display: flex;
    font-size: 24px;
    justify-content: center;
    padding: 3rem;
    max-width: 10%;
  `

  return <h3 css={style}>But should look like this!</h3>
}
export const Actual = () => {
  const style = css`
    /* stylelint-disable */
    display: flex;
    background-color: coral
    color: blue;
    justify-content: center;
    max-with: 10%;
    padding: 3rem;
    font-size: 24px;
    /* stylelint-enable */
  `

  return <h3 css={style}>This text looks like this...!</h3>
}

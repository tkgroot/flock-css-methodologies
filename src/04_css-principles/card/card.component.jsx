/** @jsx jsx */
import PropTypes from "prop-types"
import { jsx, css } from "@emotion/core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faArrowUp, faStroopwafel } from "@fortawesome/free-solid-svg-icons"

export const CardComponent = ({ title, number, percentage, negative, label }) => {
  const styles = css`
    .card {
      background-color: #fff;
      border-radius: 5px;
      display: flex;
      flex-flow: column nowrap;
      padding: 2rem;
    }

    .card__text--light {
      color: rgba(0, 0, 0, 0.4);
      font-weight: 300;
    }

    .card__text--bold {
      font-weight: 700;
    }

    .card__statistics {
      font-size: 2.5rem;
      margin-bottom: 1.25rem;
    }

    .card__header {
      align-items: center;
      display: flex;
      flex-flow: row;
      justify-content: space-between;
      margin-bottom: 1.25rem;
    }

    .card__icon {
      border-radius: 50%;
      padding: 0.5rem;
    }

    .card__icon--coral {
      background-color: coral;
    }

    .card__footer {
      display: flex;
      flex-flow: row;
      line-height: 32px;
    }

    .card__arrow {
      margin-right: 0.5rem;
    }

    .card__percentage {
      margin-right: 0.5rem;
    }
  `

  return (
    <div id="#story-wrapper" css={styles}>
      <div className="card">
        <header className="card__header">
          <span className="card__title card__text--light">{title}</span>
          <FontAwesomeIcon
            className="card__icon card__icon--coral"
            size="2x"
            icon={faStroopwafel}
          />
        </header>
        <h2 className="card__statistics card__text--bold">{number}</h2>
        <footer className="card__footer">
          <div className="card__arrow">
            {negative ? (
              <FontAwesomeIcon icon={faArrowDown} />
            ) : (
              <FontAwesomeIcon icon={faArrowUp} />
            )}
          </div>
          <span className="card__percentage">{percentage}%</span>
          <span className="card__label card__text--light">{label}</span>
        </footer>
      </div>
    </div>
  )
}

CardComponent.propTypes = {
  title: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  percentage: PropTypes.number.isRequired,
  negative: PropTypes.bool,
  label: PropTypes.string.isRequired,
}

CardComponent.defaultProps = {
  negative: false,
}

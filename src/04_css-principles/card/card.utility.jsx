/** @jsx jsx */
import PropTypes from "prop-types"
import { jsx, css } from "@emotion/core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faArrowUp, faStroopwafel } from "@fortawesome/free-solid-svg-icons"

// prettier-ignore
export const styles = css`
/* prettier-ignore */
.align-items-center { align-items: center; }
.bg-white { background-color: #fff; }
.bg-coral { background-color: coral; }
.flex { display: flex; }
.flex-row { flex-flow: row nowrap; }
.flex-column { flex-flow: column nowrap; }
.justify-content-between { justify-content: space-between; }
.p-1 { padding: 0.5rem; }
.p-2 { padding: 2rem; }
.mb-1 { margin-bottom: 1.25rem; }
.mr-1 { margin-right: 0.5rem; }
.br-5 { border-radius: 5px; }
.br-50 { border-radius: 50%; }
.color-light { color: rgba(0, 0, 0, 0.4); }
.fs-2 { font-size: 2.5rem; }
.fw-300 { font-weight: 300; }
.fw-700 { font-weight: 700; }
.lh-20 { line-height: 32px; }
`

export const CardUtility = ({ title, number, percentage, negative, label }) => {
  return (
    <div id="#story-wrapper" css={styles}>
      <div className="flex flex-column bg-white br-5 p-2">
        <div className="flex flex-row align-items-center justify-content-between mb-1">
          <span className="color-light fw-300">{title}</span>
          <FontAwesomeIcon className="bg-coral br-50 p-1" size="2x" icon={faStroopwafel} />
        </div>
        <h2 className="fs-2 fw-700 mb-1">{number}</h2>
        <div className="flex flex-row lh-20">
          <div className="mr-1">
            {negative ? (
              <FontAwesomeIcon icon={faArrowDown} />
            ) : (
              <FontAwesomeIcon icon={faArrowUp} />
            )}
          </div>
          <span className="fw-700 mr-1">{percentage}%</span>
          <span className="color-light">{label}</span>
        </div>
      </div>
    </div>
  )
}

CardUtility.propTypes = {
  title: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  percentage: PropTypes.number.isRequired,
  negative: PropTypes.bool,
  label: PropTypes.string.isRequired,
}

CardUtility.defaultProps = {
  negative: false,
}

/** @jsx jsx */
import PropTypes from "prop-types"
import { jsx, css } from "@emotion/core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faArrowUp, faStroopwafel } from "@fortawesome/free-solid-svg-icons"

export const CardInline = ({ title, number, percentage, negative, label }) => {
  return (
    <div id="#story-wrapper">
      <div
        css={css`
          background-color: #fff;
          border-radius: 5px;
          display: flex;
          flex-flow: column nowrap;
          padding: 2rem;
        `}
      >
        <div
          css={css`
            align-items: center;
            display: flex;
            flex-flow: row nowrap;
            justify-content: space-between;
            margin-bottom: 1.25rem;
          `}
        >
          <span
            css={css`
              color: rgba(0, 0, 0, 0.4);
              font-weight: 300;
            `}
          >
            {title}
          </span>
          <FontAwesomeIcon
            css={css`
              background-color: coral;
              border-radius: 50%;
              padding: 0.5rem;
            `}
            size="2x"
            icon={faStroopwafel}
          />
        </div>
        <h2
          css={css`
            font-size: 2.5rem;
            font-weight: 700;
            margin-bottom: 1.25rem;
          `}
        >
          {number}
        </h2>
        <div
          css={css`
            display: flex;
            flex-flow: row nowrap;
            line-height: 32px;
          `}
        >
          <div
            css={css`
              margin-right: 0.5rem;
            `}
          >
            {negative ? (
              <FontAwesomeIcon icon={faArrowDown} />
            ) : (
              <FontAwesomeIcon icon={faArrowUp} />
            )}
          </div>
          <span
            css={css`
              font-weight: 700;
              margin-right: 0.5rem;
            `}
          >
            {percentage}%
          </span>
          <span
            css={css`
              color: rgba(0, 0, 0, 0.4);
            `}
          >
            {label}
          </span>
        </div>
      </div>
    </div>
  )
}

CardInline.propTypes = {
  title: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  percentage: PropTypes.number.isRequired,
  negative: PropTypes.bool,
  label: PropTypes.string.isRequired,
}

CardInline.defaultProps = {
  negative: false,
}

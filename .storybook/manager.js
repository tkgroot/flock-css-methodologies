import { addons } from "@storybook/addons"
import { themes } from "@storybook/theming"
import flockTheme from "./customTheme.js"

addons.setConfig({
  theme: flockTheme,
})

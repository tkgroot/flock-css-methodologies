import { themes } from "@storybook/theming"

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  docs: {
    theme: themes.light,
  },
  layout: "padded",
  options: {
    storySort: {
      order: [
        "Introduction",
        ["Introduction", "Turns Out", "Outline", "About", "Get Started"],
        "Linting",
        [
          "Introduction",
          "Quiz",
          ["Question", "Answer"],
          "Config Controversy I",
          "Config Controversy II",
          "Idiomatic CSS",
          "Config Controversy III",
          "Vendor Prefix Order",
          "Recap",
          "Recap II",
        ],
        "Diving Into",
        ["Introduction", "Atomic Design", "Component"],
        "Principles",
        [
          "Introduction",
          "Principles",
          ["Inline", "Component", "Utility"],
          "Evaluation",
          "Evaluation II",
          "Evaluation III",
        ],
        "Surface",
        [
          "Separation of Concerns",
          "Dependency Direction",
          "CSS - HTML",
          "HTML - CSS",
          "CSS Design System",
        ],
        "Frameworks",
        ["Overview"],
        "Recap",
        ["Takeaway", "Utility-first", "Next", "Closing Remark", "Resources"],
      ],
    },
  },
}

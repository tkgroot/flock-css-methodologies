import { create } from "@storybook/theming/create"

export default create({
  base: "dark",
  brandTitle: "Flock CSS Demo",
  brandUrl: "https://flock.community",
  brandImage: "flock-icon.png",
})
